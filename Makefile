start:
	@npx next dev

start_email:
	@npx email dev -d ./src/emails -p 3001
start_docker:
	@docker run --name marc_sprachschule --network host --rm -d marc/sprachschule:latest

build:
	@npx next build
build_docker:
	@docker build -t marc/sprachschule:latest .

lint:
	@npx next lint
