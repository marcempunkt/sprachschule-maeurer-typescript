import React from "react";
import {
    Html,
    Head,
    Heading,
    Body,
    Container,
    Section,
    Img,
    Text,
    Row,
    Link,
} from "@react-email/components";
import {
    main,
    container,
    logoContainer,
    h1,
    heroText,
    footerLogos,
    footerLink,
} from "./styles";

const ConfirmationEmail: React.FC = () => {
    return (
        <Html>
            <Head />
            <Body style={main}>
                <Container style={container}>
                    <Section style={logoContainer}>
                        <Img
                            src="https://gitlab.com/marcempunkt/sprachschule-maeurerde/-/raw/master/src/assets/logo/logo_full.svg"
                            width="120"
                            alt="Sprachschule Mäurer"
                        />
                    </Section>
                    <Heading style={h1}>Deine Anfrage ist angekommen</Heading>
                    <Text style={heroText}>
                        Vielen Dank, ich werde mich so schnell wie möglich bei
                        Dir melden.
                    </Text>
                    <Section>
                        <Row style={footerLogos}>
                            <Link
                                style={footerLink}
                                href="https://sprachschule-mäurer.de"
                                target="_blank"
                                rel="noopener noreferrer">
                                Sprachschule Mäurer
                            </Link>
                        </Row>
                    </Section>
                </Container>
            </Body>
        </Html>
    );
};

export default ConfirmationEmail;
