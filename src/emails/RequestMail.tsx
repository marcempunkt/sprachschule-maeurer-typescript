import React from "react";
import {
    Html,
    Head,
    Heading,
    Body,
    Container,
    Section,
    Img,
    Text,
} from "@react-email/components";
import {
    main,
    container,
    logoContainer,
    h1,
    heroText,
    codeBox,
} from "./styles";

interface Props {
    name?: string;
    email?: string;
    message?: string;
    classType?: string;
}

const RequestEmail: React.FC<Props> = (props: Props) => {
    return (
        <Html>
            <Head />
            <Body style={main}>
                <Container style={container}>
                    <Section style={logoContainer}>
                        <Img
                            src="https://gitlab.com/marcempunkt/sprachschule-maeurerde/-/raw/master/src/assets/logo/logo_full.svg"
                            width="120"
                            alt="Sprachschule Mäurer"
                        />
                    </Section>
                    <Heading style={h1}>
                        Neue Anfrage: {props.classType || "unknown"}
                    </Heading>
                    <Text style={heroText}>
                        Name: {props.name || "unknown"}
                    </Text>
                    <Text style={heroText}>
                        Email: {props.email || "unknown"}
                    </Text>
                    <Section style={codeBox}>
                        <Text style={heroText}>
                            {props.message || "unknown"}
                        </Text>
                    </Section>
                </Container>
            </Body>
        </Html>
    );
};

export default RequestEmail;
