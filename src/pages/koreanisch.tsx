import KoreanPage from "../components/pages/KoreanPage";
import Reviews from "../components/Reviews";

export default function Koreanisch() {
    return (
        <>
            <KoreanPage />
            <Reviews />
        </>
    );
}
