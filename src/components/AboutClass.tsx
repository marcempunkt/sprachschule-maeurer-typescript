import React from "react";
import Reading from "../assets/undraw/undraw_studying.svg";
import Conversation from "../assets/undraw/undraw_conversation.svg";
import Textbox from "./Textbox";

const AboutClass: React.FC = () => {
    return (
        <>
            <Textbox
                title="Was bekomme ich vom Präsenzunterricht?"
                content="Der Unterricht findet bei Dir zu Hause statt oder nach Absprache an einem anderen Ort. Du musst keine neuen Bücher oder Workbooks kaufen. Das einzige was Du brauchst ist einen College Block, einen Stift und Freude an der japanischen Sprache."
                illustration={<Conversation />}
            />
            <Textbox
                title="Ist Japanisch zu schwer?"
                content="Nein! Japanisch hat im Gegensatz zu Deutsch oder Englisch viele komplizierte Systeme nicht, wie z.B. Geschlechter, Mehrzahl oder Fälle. Und es gibt auch nur zwei Zeitformen! Vorallem schnell auf Japanisch sprechen zu lernen ist überraschend einfach!"
                illustration={<Reading />}
                inverted={true}
            />
            <Textbox
                title="Worauf wartest Du noch?"
                content={
                    <p className="leading-loose">
                        Schicke mir nur noch eine Email mit Deinem <b>Namen</b>,
                        Deiner <b>Email</b>, <b>wo</b> und <b>wann</b> Du den
                        Japanisch Unterricht gerne hättest und ich werde mich
                        schnellst möglich bei Dir melden.
                    </p>
                }
            />
        </>
    );
};

export default AboutClass;
