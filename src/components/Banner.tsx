import React, { useState, useEffect, MouseEvent } from "react";
import { X, Gift } from "react-feather";

const Banner: React.FC = () => {
    const [show, setShow] = useState<boolean>(false);

    function handleClick(e: MouseEvent<HTMLElement>) {
        e.stopPropagation();
        console.log("handleClick")
        scrollToContact();
    }

    function handleClose(e: MouseEvent<SVGElement>) {
        e.stopPropagation();
        console.log("handleClose")
        setShow(false);
        window.localStorage.setItem("show-banner", "false");
        scrollToContact();
    }

    function scrollToContact() {
         const contact: HTMLDivElement | null =
            document.querySelector("#contact");
        if (contact) contact.scrollIntoView(true);
    }

    useEffect(() => {
        const showBanner: string | null =
            window.localStorage.getItem("show-banner");
        setShow(showBanner === "false" ? false : true);
    }, []);

    if (!show) return null;

    return (
        <button
            onClick={handleClick}
            className="fixed inset-x-0 bottom-0 cursor-pointer select-none sm:px-6 sm:pb-5 lg:px-8">
            <div className="flex items-center justify-between bg-gray-900/90 px-6 py-2.5 transition-colors hover:bg-gray-900 sm:rounded-xl sm:py-3 sm:pl-4 sm:pr-3.5">
                <p className="flex items-center gap-3 font-semibold text-white">
                    <Gift />
                    Jetzt Gratisstunde sichern!
                </p>
                <button
                    type="button"
                    className="-m-3 flex-none p-3 focus-visible:outline-offset-[-4px]">
                    <span className="sr-only">Dismiss</span>
                    <X
                        onClick={handleClose}
                        className="z-10 h-5 w-5 text-white"
                        aria-hidden="true"
                    />
                </button>
            </div>
        </button>
    );
};

export default Banner;
