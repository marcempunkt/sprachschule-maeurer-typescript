import React from "react";
import Nav from "../Nav";
import Landing from "../Landing";
import ChooseLanguage from "../ChooseLanguage";
import Offer from "../Offer";
import AboutMe from "../AboutMe";
import AboutClass from "../AboutClass";
import Contact from "../contact/Contact";
import Banner from "../Banner"

const JapanesePage: React.FC = () => {
    return (
        <>
            <Nav />
            <Landing />
            <ChooseLanguage />
            <Offer />
            <AboutMe />
            <AboutClass />
            <Contact />
            {/* <Banner /> */}
        </>
    );
};

export default JapanesePage;
