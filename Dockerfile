FROM node:lts AS builder

WORKDIR /usr/src/sprachschule/
COPY ./ ./

RUN npm install
RUN make build

FROM nginx:stable AS server

COPY --from=builder /usr/src/sprachschule/build/ /usr/share/nginx/html/
COPY ./Dockerfile.nginx.conf /etc/nginx/nginx.conf

EXPOSE 80
